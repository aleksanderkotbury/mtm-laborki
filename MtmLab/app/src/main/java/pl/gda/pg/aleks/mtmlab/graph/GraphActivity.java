package pl.gda.pg.aleks.mtmlab.graph;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import pl.gda.pg.aleks.mtmlab.R;
import pl.gda.pg.aleks.mtmlab.SensorEventPublisher;

public class GraphActivity extends AppCompatActivity {

    @BindView(R.id.graph)
    public GraphView graphView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        ButterKnife.bind(this);
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(100);

        LineGraphSeries<DataPoint> seriesX = new LineGraphSeries<>();
        LineGraphSeries<DataPoint> seriesY = new LineGraphSeries<>();
        LineGraphSeries<DataPoint> seriesZ = new LineGraphSeries<>();
        graphView.addSeries(seriesX);
        graphView.addSeries(seriesY);
        graphView.addSeries(seriesZ);

        Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorMagnetic, 1000000);
        })
                .subscribe(event -> {
                    float[] values = event.values;
                    long x = System.currentTimeMillis();
                    DataPoint xData = new DataPoint(x, values[0]);
                    DataPoint yData = new DataPoint(x, values[1]);
                    DataPoint zData = new DataPoint(x, values[2]);

                    seriesX.appendData(xData, true, 100);
                    seriesY.appendData(yData, true, 100);
                    seriesZ.appendData(zData, true, 100);
                });
    }
}
