package pl.gda.pg.aleks.mtm_lab3.record;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.gda.pg.aleks.mtm_lab3.R;
import rx.Emitter;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AudioRecorderActivity extends AppCompatActivity {

    @BindView(R.id.audio_record_button)
    Button recordButton;
    @BindView(R.id.audio_stop_record_button)
    Button stopRecordingButton;
    @BindView(R.id.audio_view)
    AudioView audioView;

    private AudioRecord audioRecord;
    private Subscription subscribe;

    private List<short[]> samples = new LinkedList<>();
    private int minBuffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_recorder);
        ButterKnife.bind(this);

        minBuffer = AudioRecord.getMinBufferSize(44100,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
    }

    @OnClick(R.id.audio_record_button)
    public void recordSound() {
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, 44100,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, minBuffer);
        audioRecord.startRecording();
        subscribe = Observable.<Float>fromEmitter(emitter -> createEmitter(audioRecord, emitter), Emitter.BackpressureMode.LATEST)
                .throttleLast(100, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(audioView::addSample, t -> Log.e(getClass().getSimpleName(), "error during subscription", t));

        stopRecordingButton.setEnabled(true);
        recordButton.setEnabled(false);
    }

    private void createEmitter(AudioRecord audioRecord, Emitter<Float> emitter) {
        while (true) {
            short[] bytes = new short[minBuffer / 4];
            int read = audioRecord.read(bytes, 0, minBuffer / 4);
            if (read < 0) {
                break;
            }
            Log.i(getClass().getSimpleName(), "emitting data");

            float average = average(bytes);

            emitter.onNext(average);
        }
    }

    private float average(short[] bytes) {
        long sum = 0;
        for (short d : bytes) {
            int sample = d;
            if(d < 0) {
                sample = -1 * d + Short.MAX_VALUE;
            }
            sum += sample;
        }
        return sum / bytes.length;
    }

    @OnClick(R.id.audio_stop_record_button)
    public void stopRecording() {
        if (subscribe != null) {
            subscribe.unsubscribe();
        }
        audioRecord.stop();
        audioRecord.release();
        stopRecordingButton.setEnabled(false);
        recordButton.setEnabled(true);
    }
}
