package pl.gda.pg.aleks.mtmcamera.video;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.gda.pg.aleks.mtmcamera.R;

public class VideoActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{

    @BindView(R.id.cameraView)
    SurfaceView cameraView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            return;
        }
        cameraView.getHolder().addCallback(SurfaceCallbackFactory.create(this::init));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        init();
    }

    private void init() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String id = manager.getCameraIdList()[0];
            CameraOpenCallback cameraOpenCallback = new CameraOpenCallback(cameraView);
            manager.openCamera(id, cameraOpenCallback, new Handler());
        } catch (CameraAccessException e) {
            Log.e(getClass().getName(), "ee", e);
        }
    }
}
