package pl.gda.pg.aleks.mtmlab.bounce;

import static pl.gda.pg.aleks.mtmlab.bounce.BounceActivity.RADIUS;

/**
 * Created by aleks on 26.11.2016.
 */

public class Position {
    int x = RADIUS;
    int y = RADIUS;

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (x != position.x) return false;
        return y == position.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
