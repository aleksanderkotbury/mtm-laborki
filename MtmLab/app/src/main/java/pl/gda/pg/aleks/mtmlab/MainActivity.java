package pl.gda.pg.aleks.mtmlab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.gda.pg.aleks.mtmlab.accelerometer.AccelerometerActivity;
import pl.gda.pg.aleks.mtmlab.bounce.BounceActivity;
import pl.gda.pg.aleks.mtmlab.compas.CompasActivity;
import pl.gda.pg.aleks.mtmlab.graph.GraphActivity;
import pl.gda.pg.aleks.mtmlab.sensors.SensorsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.accelerometer_button)
    public void accelerometer() {
        Intent intent = new Intent(this, AccelerometerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.all_sensors_button)
    public void allSensors() {
        Intent intent = new Intent(this, SensorsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.graph)
    public void graph() {
        Intent intent = new Intent(this, GraphActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.compas_button)
    public void compas() {
        Intent intent = new Intent(this, CompasActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.bounce_button)
    public void bounce() {
        Intent intent = new Intent(this, BounceActivity.class);
        startActivity(intent);
    }
}
