package pl.gda.pg.aleks.mtmlab.accelerometer;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;

import java.util.ArrayDeque;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import pl.gda.pg.aleks.mtmlab.R;
import pl.gda.pg.aleks.mtmlab.SensorEventPublisher;

public class AccelerometerActivity extends AppCompatActivity {

    @BindView(R.id.x_axis_value)
    public AppCompatTextView xValue;
    @BindView(R.id.y_axis_value)
    public AppCompatTextView yValue;
    @BindView(R.id.z_axis_value)
    public AppCompatTextView zValue;
    @BindView(R.id.screen_orientation_value)
    public AppCompatTextView screenOrientationValue;
    @BindView(R.id.is_shaking)
    public AppCompatTextView isShaking;
    public Avarege avarege = new Avarege();

    private Queue<SensorEvent> events = new ArrayDeque<>();
    long[] time = new long[]{0L};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);
        ButterKnife.bind(this);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorAccelerometer, 100000);
        }).subscribe(event -> {
            float[] values = event.values;
            xValue.setText(Float.toString(values[0]));
            yValue.setText(Float.toString(values[1]));
            zValue.setText(Float.toString(values[2]));

            events.add(event);
            int size = events.size();
            if (size == 10) {
                float dx = 0;
                float dy = 0;
                float dz = 0;
                for (SensorEvent s : events) {
                    float[] currentValues = s.values;
                    dx += normalize(currentValues[0]);
                    dy += normalize(currentValues[1]);
                    dz += normalize(currentValues[2]);
                }
                float result = dx + dy + dz;
                float av = result / 10;
                float diff = normalize(av - avarege.avarege);
                if (diff > 1) {
                    isShaking.setText("YES!!");
                    time[0] = event.timestamp;
                } else if (time[0] != 0) {
                    if (event.timestamp - time[0] > 5_000_000_000L) {
                        isShaking.setText("NO :(");
                    }
                }

                avarege.addNew(av);
                Log.i("result", "" + avarege.avarege + " " + diff);
                events.poll();
            }

            int best = getBiggestUnsigned(values[0], values[1], values[2]);
            if (best == 0) {
                if (values[best] > 0) {
                    screenOrientationValue.setText("LEFT!");
                } else {
                    screenOrientationValue.setText("RIGHT!");
                }
            } else if (best == 1) {
                if (values[best] > 0) {
                    screenOrientationValue.setText("STRAIGHT!");
                } else {
                    screenOrientationValue.setText("FLIPPED!");
                }

            } else {
                if (values[2] < 0) {
                    screenOrientationValue.setText("DOWN!");
                } else {
                    screenOrientationValue.setText("UP!");
                }
            }
        });
    }

    private int getBiggestUnsigned(float value, float value1, float value2) {
        float normalize = normalize(value);
        float normalize1 = normalize(value1);
        float normalize2 = normalize(value2);

        if (normalize > normalize1 && normalize > normalize2) {
            return 0;
        } else if (normalize1 > normalize2) {
            return 1;
        }
        return 2;
    }

    private float normalize(float f) {
        if (f >= 0) {
            return f;
        }
        return f * -1;
    }
}
