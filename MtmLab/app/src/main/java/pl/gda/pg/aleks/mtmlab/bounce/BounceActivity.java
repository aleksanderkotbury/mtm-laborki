package pl.gda.pg.aleks.mtmlab.bounce;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import pl.gda.pg.aleks.mtmlab.R;

public class BounceActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    public static final int RADIUS = 25;
    @BindView(R.id.bounce_surface)
    public SurfaceView surfaceView;
    private Position position;
    private Paint paint;
    private Paint black;
    private Paint red;
    private Position metaPosition;

    private Disposable[] disposables = new Disposable[1];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bounce);
        ButterKnife.bind(this);
        surfaceView.getHolder().addCallback(this);

        position = new Position();
        paint = new Paint();
        paint.setColor(Color.WHITE);
        black = new Paint();
        black.setColor(Color.BLACK);
        red = new Paint();
        red.setColor(Color.RED);

    }

    private void drawCircle(SurfaceHolder holder) {
        Canvas canvas = holder.lockCanvas();
        canvas.drawCircle(position.x, position.y, RADIUS * 3, black);
        canvas.drawCircle(position.x, position.y, RADIUS, paint);
        canvas.drawCircle(metaPosition.x, metaPosition.y, RADIUS, red);
        holder.unlockCanvasAndPost(canvas);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Canvas canvas = holder.lockCanvas();
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        holder.unlockCanvasAndPost(canvas);

        Log.i(getClass().getSimpleName(), "surface created");
        Random random = new Random();
        int randWidth = random.nextInt(width - RADIUS * 2) + RADIUS;
        int randHeight = random.nextInt(height - RADIUS * 2) + RADIUS;
        metaPosition = new Position();
        metaPosition.x = randWidth;
        metaPosition.y = randHeight;

        drawCircle(holder);

        disposables[0] = new RotationObservable().toObservable(this)
//                .throttleLast(50, TimeUnit.MILLISECONDS)
                .subscribe(event -> {
                    float[] values = event.values;
                    float vX = values[1] * 10;
                    float vY = values[0] * 10;
                    if (this.position.x > RADIUS && values[1] < 0 || this.position.x < width && values[1] > 0) {
                        this.position.x += vX;
                    }
                    if (this.position.x + vX < RADIUS || this.position.x + vX > width - RADIUS) {
                        this.position.x -= vX * 2;
                    }
                    if (this.position.y > RADIUS && values[0] < 0 || this.position.y < height && values[0] > 0) {
                        this.position.y += vY;
                    }
                    if (this.position.y + vY < RADIUS || this.position.y + vY > height - RADIUS) {
                        this.position.y -= vY * 2;
                    }
                    drawCircle(holder);
                    int dx = position.x - metaPosition.x;
                    int dy = position.y - metaPosition.y;
                    double distance = Math.sqrt(dx * dx + dy * dy);
//                    Log.i(getClass().getSimpleName(), "" + distance);
                    if (distance < RADIUS * 0.5) {
                        disposables[0].dispose();
                        disposables[0] = null;
                    }
                });
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
