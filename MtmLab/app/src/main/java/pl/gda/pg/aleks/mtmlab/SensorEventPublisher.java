package pl.gda.pg.aleks.mtmlab;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

import org.reactivestreams.Subscriber;

/**
 * Created by aleks on 26.11.2016.
 */

public class SensorEventPublisher implements SensorEventListener {

    private final Subscriber<? super SensorEvent> sensorEventSubscriber;

    public SensorEventPublisher(Subscriber<? super SensorEvent> sensorEventSubscriber) {
        this.sensorEventSubscriber = sensorEventSubscriber;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        sensorEventSubscriber.onNext(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
