package pl.gda.pg.aleks.mtmcamera.video;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;
import android.util.Log;

/**
 * Created by aleks on 26.11.2016.
 */

public class CaptureSession extends CameraCaptureSession.StateCallback {
    private final CaptureRequest captureRequest;

    public CaptureSession(CaptureRequest captureRequest) {
        this.captureRequest = captureRequest;
    }

    @Override
    public void onConfigured(CameraCaptureSession session) {
        try {
            session.setRepeatingRequest(captureRequest, null, new Handler());
        } catch (CameraAccessException e) {
            Log.e(getClass().getName(), "ee", e);
        }
    }

    @Override
    public void onConfigureFailed(CameraCaptureSession session) {

    }
}
