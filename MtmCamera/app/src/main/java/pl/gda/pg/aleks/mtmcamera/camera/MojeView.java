package pl.gda.pg.aleks.mtmcamera.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;

public class MojeView extends View {

    public static final double VERTICAL_TANGENS = Math.tan(Math.toRadians(47.1 / 2));
    public static final double HORIZONTAL_TANGENS = Math.tan(Math.toRadians(60.8 / 2));
    private float[] dane = null;
    private float[] up;
    private float[] dol;
    private float[] wschod;
    private float[] zachod;
    private float[] polnoc;
    private float[] poludnie;
    private float[] warsaw;

    Paint p;

    public void setWschod(float[] wschod) {
        this.wschod = wschod;
    }

    public void setZachod(float[] zachod) {
        this.zachod = zachod;
    }

    public void setPolnoc(float[] polnoc) {
        this.polnoc = polnoc;
    }

    public void setPoludnie(float[] poludnie) {
        this.poludnie = poludnie;
    }

    public void setDol(float[] dol) {
        this.dol = dol;
    }

    public void setWarsaw(float[] warsaw) {
        this.warsaw = warsaw;
    }

    public void setDane(float[] dane) {
        this.dane = dane;
        j++;
    }

    public void setUp(float[] up) {
        this.up = up;
    }

    private int i = 0;
    private int j = 0;

    public MojeView(Context context) {
        super(context);
        p = new Paint();
        p.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        p.setColor(Color.RED);
        canvas.drawText("MojeView.onDraw: i=" + i++, 200, 100, p);
        canvas.drawText("MojeView.onDraw: j=" + j++, 200, 150, p);
//        if (dane != null) {
//            canvas.drawLine(0, 0, 100 * dane[0], 100 * dane[0], p);
//            p.setStyle(Style.STROKE);
//            canvas.drawRect(100, 100, 200, 200, p);
//
//        }
            p.setStyle(Paint.Style.FILL);
            if (up != null && up[2] < 0) {
                p.setColor(Color.WHITE);
                float y = 672 + (float) (up[0] / up[2] / VERTICAL_TANGENS * 672);
                float x = 896 + (float) (up[1] / up[2] / HORIZONTAL_TANGENS * 896);
                canvas.drawCircle(x, y, 25, p);
            }
            if (dol != null && dol[2] < 0) {
                p.setColor(Color.BLACK);
            float y = 672 + (float) (dol[0] / dol[2] / VERTICAL_TANGENS * 672);
            float x = 896 + (float) (dol[1] / dol[2] / HORIZONTAL_TANGENS * 896);
            canvas.drawCircle(x, y, 25, p);
        }
        if (polnoc != null && polnoc[2] < 0) {
            p.setColor(Color.BLUE);

            float y = 672 + (float) (polnoc[0] / polnoc[2] / VERTICAL_TANGENS * 672);
            float x = 896 + (float) (polnoc[1] / polnoc[2] / HORIZONTAL_TANGENS * 896);
            canvas.drawCircle(x, y, 25, p);
        }
        if (poludnie != null && poludnie[2] < 0) {
            p.setColor(Color.RED);

            float y = 672 + (float) (poludnie[0] / poludnie[2] / VERTICAL_TANGENS * 672);
            float x = 896 + (float) (poludnie[1] / poludnie[2] / HORIZONTAL_TANGENS * 896);
            canvas.drawCircle(x, y, 25, p);
        }
        if (wschod != null && wschod[2] < 0) {
            p.setColor(Color.YELLOW);

            float y = 672 + (float) (wschod[0] / wschod[2] / VERTICAL_TANGENS * 672);
            float x = 896 + (float) (wschod[1] / wschod[2] / HORIZONTAL_TANGENS * 896);
            canvas.drawCircle(x, y, 25, p);
        }
        if (zachod != null && zachod[2] < 0) {
            p.setColor(Color.GREEN);

            float y = 672 + (float) (zachod[0] / zachod[2] / VERTICAL_TANGENS * 672);
            float x = 896 + (float) (zachod[1] / zachod[2] / HORIZONTAL_TANGENS * 896);
            canvas.drawCircle(x, y, 25, p);
        }
        if (warsaw != null && warsaw[2] < 0) {
            p.setColor(Color.CYAN);

            float y = 672 + (float) (warsaw[0] / warsaw[2] / VERTICAL_TANGENS * 672);
            float x = 896 + (float) (warsaw[1] / warsaw[2] / HORIZONTAL_TANGENS * 896);
            canvas.drawCircle(x, y, 25, p);
        }
    }


}
