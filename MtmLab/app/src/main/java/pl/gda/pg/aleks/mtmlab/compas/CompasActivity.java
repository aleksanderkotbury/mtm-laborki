package pl.gda.pg.aleks.mtmlab.compas;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import pl.gda.pg.aleks.mtmlab.R;
import pl.gda.pg.aleks.mtmlab.SensorEventPublisher;

public class CompasActivity extends AppCompatActivity {

    @BindView(R.id.compas_view)
    public CompasView compasView;
    @BindView(R.id.surfaceView)
    public SurfaceView surfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compas);
        ButterKnife.bind(this);
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Sensor sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        int period = 1000;
        Observable<SensorEvent> magneticObservable = Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorMagnetic, period);
        });

        Observable<SensorEvent> gravityObservable = Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorGravity, period);
        });
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);

        Observable.zip(magneticObservable, gravityObservable, this::countRotation)
                .subscribe(r -> {
                    double xb = r[3] / Math.sqrt(r[3] * r[3] + r[4] * r[4]);
                    double yb = r[4] / Math.sqrt(r[3] * r[3] + r[4] * r[4]);
                    Canvas canvas = surfaceView.getHolder().lockCanvas();
                    int width = canvas.getWidth();
                    int height = canvas.getHeight();
                    int len = width / 2;
                    canvas.drawLine(width, height, (float) (20 + width * len), (float) (height + yb * len), paint);
                    Log.i(getClass().getSimpleName(), "xb: " + xb + " yb: " + yb + " r: " + Arrays.toString(r));
                });
    }

    private float[] countRotation(SensorEvent magneticEvent, SensorEvent gravityEvent) {
        float[] rotation = new float[9];
        SensorManager.getRotationMatrix(rotation, null, gravityEvent.values, magneticEvent.values);
        return rotation;
    }
}
