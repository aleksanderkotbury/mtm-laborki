package pl.gda.pg.aleks.mtm_lab3.record;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.LinkedList;

/**
 * Created by aleks on 07.01.2017.
 */

public class AudioView extends View {

    public static final int MAX_VALUE = 2 * Short.MAX_VALUE;
    private short[] data = new short[0];
    private Paint paint = new Paint();

    private final LinkedList<Float> samples = new LinkedList<>();

    public AudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(30);
    }

    public void setSamples(short[] samples) {
        this.data = samples;
        Log.i(getClass().getSimpleName(), "receiving data");
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (samples.size() == 0) {
            return;
        }

        int width = canvas.getWidth();
        int height = canvas.getHeight();
        float ratio = height * 1.0f / MAX_VALUE;

        int length = samples.size();
        float sampleWidth = width * 1.0f / length;
        float startX = 0;
        for (float sample : samples) {
            float y = sample * ratio;
            canvas.drawLine(startX, height, startX, height - y, paint);
            startX += sampleWidth;
        }
    }

    public void addSample(Float sample) {
        Log.i(getClass().getSimpleName(), "receiving sample: " + sample);
        samples.addLast(sample);
        if (samples.size() > 50) {
            samples.removeFirst();
        }
        invalidate();
    }
}
