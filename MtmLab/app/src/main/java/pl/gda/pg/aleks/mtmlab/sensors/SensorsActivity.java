package pl.gda.pg.aleks.mtmlab.sensors;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.gda.pg.aleks.mtmlab.R;

public class SensorsActivity extends AppCompatActivity {

    @BindView(R.id.sensors_list)
    public ListView sensorsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);
        ButterKnife.bind(this);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        SensorAdapter sensorAdapter = new SensorAdapter(sensorList, this);
        sensorsList.setAdapter(sensorAdapter);
        sensorsList.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(SensorsActivity.this, SingleSensorActivity.class);
            Sensor sensor = sensorList.get(position);
            Bundle bundle = new Bundle();
            int type = sensor.getType();
            bundle.putInt(SingleSensorActivity.SENSOR_TYPE, type);
            intent.putExtras(bundle);
            startActivity(intent);
        });
    }
}
