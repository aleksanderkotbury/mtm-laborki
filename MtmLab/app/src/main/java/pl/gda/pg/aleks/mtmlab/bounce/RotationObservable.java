package pl.gda.pg.aleks.mtmlab.bounce;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import org.reactivestreams.Subscriber;

import io.reactivex.Flowable;
import pl.gda.pg.aleks.mtmlab.SensorEventPublisher;

public class RotationObservable {

    public Flowable<SensorEvent> toObservable(Context context) {
        return Flowable.<SensorEvent>fromPublisher(emitter -> listenForRotation(emitter, context));
    }

    private void listenForRotation(Subscriber<? super SensorEvent> emitter, Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        SensorEventPublisher sensorEventPublisher = new SensorEventPublisher(emitter);
        sensorManager.registerListener(sensorEventPublisher, rotationSensor, 50000);
    }
}
