package pl.gda.pg.aleks.mtm_lab3;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.VideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.gda.pg.aleks.mtm_lab3.record.AudioRecorderActivity;
import pl.gda.pg.aleks.mtm_lab3.record.RecordActivity;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.videoView)
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) || checkPermission(Manifest.permission.RECORD_AUDIO) || checkPermission(Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA}, 1);
        }
    }

    private boolean checkPermission(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED;
    }

    @OnClick(R.id.record_activity_button)
    public void record() {
        Intent intent = new Intent(this, RecordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.audio_recorder_intent)
    public void recordAudio() {
        Intent intent = new Intent(this, AudioRecorderActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.record_video_intent)
    public void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK)
                    handleVideo(data);
                break;
            default:
                break;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleVideo(Intent data) {
        Uri uri = data.getData();
        videoView.setVideoURI(uri);
        videoView.start();
    }
}
