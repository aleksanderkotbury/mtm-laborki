package pl.gda.pg.aleks.mtmcamera.camera;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.RelativeLayout;

import io.reactivex.Observable;
import pl.gda.pg.aleks.mtmcamera.R;
import pl.gda.pg.aleks.mtmcamera.SensorEventPublisher;

public class KameraAugmentedActivity extends Activity implements SensorEventListener {
    /**
     * Called when the activity is first created.
     */

    RelativeLayout rl;
    SensorManager sm;
    MojeView myCameraOverlay;
    Preview myCameraView;

    float usX = (float) Math.toRadians(54);
    float usY = (float) Math.toRadians(18);
    float warsawX = (float) Math.toRadians(52);
    float warsawY = (float) Math.toRadians(21);
    float[] toWarsaw = latlonToENU(warsawX, warsawY, 10, usX, usY, 10);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            return;
        }

        rl = (RelativeLayout) findViewById(R.id.relativeLayout1);

        myCameraView = new Preview(this);
        rl.addView(myCameraView);

        myCameraOverlay = new MojeView(this);
        rl.addView(myCameraOverlay);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        float[] namiarUp = new float[]{0.0F, 0, 1000};
        float[] namiarPolnoc = new float[]{0.0F, 100, 0};
        float[] namiarPoludnie = new float[]{0.0F, -100, 0};
        float[] namiarWschod = new float[]{100, 0, 0};
        float[] namiarZachod = new float[]{-100, 0, 0};
        float[] namiarDol = new float[]{0, 0, -1000};


        sm = (SensorManager) getSystemService(SENSOR_SERVICE);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Sensor sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        int period = 100000;
        Observable<SensorEvent> magneticObservable = Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorMagnetic, period);
        });

        Observable<SensorEvent> gravityObservable = Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorGravity, period);
        });

        Observable.zip(magneticObservable, gravityObservable, this::countRotation)
                .subscribe(r -> {
                    float[] up = getFloats(r, namiarUp);
                    float[] polnoc = getFloats(r, namiarPolnoc);
                    float[] poludnie = getFloats(r, namiarPoludnie);
                    float[] wschod = getFloats(r, namiarWschod);
                    float[] zachod = getFloats(r, namiarZachod);
                    float[] dol = getFloats(r, namiarDol);
                    float[] warsaw = getFloats(r, toWarsaw);
//					Log.i(getClass().getSimpleName(), Arrays.toString(namiarB));
                    myCameraOverlay.setUp(up);
                    myCameraOverlay.setPolnoc(polnoc);
                    myCameraOverlay.setPoludnie(poludnie);
                    myCameraOverlay.setWschod(wschod);
                    myCameraOverlay.setZachod(zachod);
                    myCameraOverlay.setDol(dol);
                    myCameraOverlay.setWarsaw(warsaw);
                    myCameraOverlay.invalidate();
                });
    }

    private float[] getFloats(float[] r, float[] namiarM) {
        float[] result = new float[3];
        result[0] = namiarM[0] * r[0] + namiarM[1] * r[3] + namiarM[2] * r[6];
        result[1] = namiarM[0] * r[1] + namiarM[1] * r[4] + namiarM[2] * r[7];
        result[2] = namiarM[0] * r[2] + namiarM[1] * r[5] + namiarM[2] * r[8];
        return result;
    }

    private float[] countRotation(SensorEvent magneticEvent, SensorEvent gravityEvent) {
        float[] rotation = new float[9];
        SensorManager.getRotationMatrix(rotation, null, gravityEvent.values, magneticEvent.values);
        return rotation;
    }

    static final float EARTH_RADIUS = 6378137; //lat – szerokość i lon – długość geograficzna w radianach

    static float[] latLonToECEF(float lat, float lon, float height) {
        float[] ECEF = new float[3];
        ECEF[0] = (float) ((height + EARTH_RADIUS) * Math.cos(lat) * Math.cos(lon));
        ECEF[1] = (float) ((height + EARTH_RADIUS) * Math.cos(lat) * Math.sin(lon));
        ECEF[2] = (float) ((height + EARTH_RADIUS) * Math.sin(lat));
        return ECEF;
    }

    static float[] latlonToENU(float xLat, float xLon, float xHeight, float uLat, float uLon, float uHeight) {
        float[] ecefX = latLonToECEF(xLat, xLon, xHeight);
        float[] ecefU = latLonToECEF(uLat, uLon, uHeight);
        float[] offsetECEF = new float[3];
        offsetECEF[0] = ecefX[0] - ecefU[0];
        offsetECEF[1] = ecefX[1] - ecefU[1];
        offsetECEF[2] = ecefX[2] - ecefU[2];
        float[] enu = new float[3];
        enu[0] = (float) (-Math.sin(uLon) * offsetECEF[0] + Math.cos(uLon) * offsetECEF[1]);
        enu[1] = (float) (-Math.cos(uLon) * Math.sin(uLat) * offsetECEF[0] - Math.sin(uLon) * Math.sin(uLat) * offsetECEF[1] + Math.cos(uLat) * offsetECEF[2]);
        enu[2] = (float) (Math.cos(uLon) * Math.cos(uLat) * offsetECEF[0] + Math.sin(uLon) * Math.cos(uLat) * offsetECEF[1] + Math.sin(uLat) * offsetECEF[2]);
        return enu;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Sensor def = sm.getDefaultSensor(Sensor.TYPE_ALL);
        sm.registerListener(this, def, SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        myCameraOverlay.setDane(event.values);
        myCameraView.invalidate();
    }
}