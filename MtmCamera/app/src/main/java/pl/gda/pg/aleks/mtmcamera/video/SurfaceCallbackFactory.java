package pl.gda.pg.aleks.mtmcamera.video;

import android.view.SurfaceHolder;

public class SurfaceCallbackFactory {
    static SurfaceHolder.Callback create(Action action){
        return new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                action.doAction();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        };
    }

}
