package pl.gda.pg.aleks.mtmcamera.video;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;

import java.util.Arrays;

/**
 * Created by aleks on 26.11.2016.
 */

public class CameraOpenCallback extends CameraDevice.StateCallback {
    private final SurfaceView cameraView;

    public CameraOpenCallback(SurfaceView cameraView) {
        this.cameraView = cameraView;
    }

    @Override
    public void onOpened(CameraDevice camera) {
        Surface viewSurface = cameraView.getHolder().getSurface();
        try {
            CaptureRequest.Builder captureRequest = camera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            captureRequest.addTarget(viewSurface);
            camera.createCaptureSession(Arrays.asList(viewSurface), new CaptureSession(captureRequest.build()), new Handler());
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDisconnected(CameraDevice camera) {
        Log.i(getClass().getSimpleName(), "camera disconnected");
    }

    @Override
    public void onError(CameraDevice camera, int error) {
        Log.e(getClass().getSimpleName(), "error code: " + error);
    }
}
