package pl.gda.pg.aleks.mtmcamera.video;

/**
 * Created by aleks on 13.11.2016.
 */

public interface Action {
    void doAction();
}
