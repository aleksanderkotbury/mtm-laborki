package pl.gda.pg.aleks.mtmlab.compas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by aleks on 26.11.2016.
 */

public class CompasView extends View {

    private double dx = 0;
    private double dy = 0;
    private Paint black = new Paint();

    public CompasView(Context context) {
        super(context);
    }

    public CompasView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CompasView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CompasView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setDx(double dx) {
        this.dx = dx;
    }

    public void setDy(double dy) {
        this.dy = dy;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(20, 20, (float) (20 + dx), (float) (20 + dy), black);
    }
}
