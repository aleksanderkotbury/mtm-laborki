package pl.gda.pg.aleks.mtmcamera.camera;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

class Preview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera camera;

    Preview(Context context) {
        super(context);
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        camera = Camera.open();
        try {
            camera.setPreviewDisplay(holder);
            List<Camera.Size> supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
            Camera.Size pictureSize = supportedPreviewSizes.get(0);
            camera.getParameters().setPictureSize(pictureSize.width, pictureSize.height);
//            float horizontalViewAngle = camera.getParameters().getHorizontalViewAngle();
//            float verticalViewAngle = camera.getParameters().getVerticalViewAngle();
//            Log.i(getClass().getSimpleName(), pictureSize.height + " " + pictureSize.width + " " + horizontalViewAngle + " " + verticalViewAngle);
            mHolder.setFixedSize(pictureSize.width, pictureSize.height);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.release();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    }
}