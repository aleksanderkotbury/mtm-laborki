package pl.gda.pg.aleks.mtm_lab3.record;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.gda.pg.aleks.mtm_lab3.R;

public class RecordActivity extends AppCompatActivity {

    private final File rootDir = Environment.getExternalStorageDirectory();
    private MediaRecorder mediaRecorder;

    @BindView(R.id.play_button)
    Button playButton;
    @BindView(R.id.stop_playing_button)
    Button stopPlayingButton;
    @BindView(R.id.record_button)
    Button recordButton;
    @BindView(R.id.stop_record_button)
    Button stopRecordingButton;

    private File actualFile = null;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        ButterKnife.bind(this);
        mediaRecorder = new MediaRecorder();
    }

    @OnClick(R.id.record_button)
    public void recordSound(){
        File tempFile = null;
        try {
            tempFile = File.createTempFile("recording", "3gpp", rootDir);
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "error creating temporary file", e);
            return;
        }
        mediaRecorder.setAudioChannels(1);
        mediaRecorder.setAudioSamplingRate(8000);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(tempFile.getAbsolutePath());
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "error preparing media recorder", e);
            return;
        }
        mediaRecorder.start();

        stopRecordingButton.setEnabled(true);
        recordButton.setEnabled(false);
        playButton.setEnabled(false);
        stopPlayingButton.setEnabled(false);
        actualFile = tempFile;
    }

    @OnClick(R.id.stop_record_button)
    public void stopRecording() {
        mediaRecorder.stop();
        stopRecordingButton.setEnabled(false);
        recordButton.setEnabled(true);
        playButton.setEnabled(true);
        stopPlayingButton.setEnabled(false);
    }

    @OnClick(R.id.play_button)
    public void startPlaying() {
        if (actualFile == null) {
            return;
        }

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(actualFile.getAbsolutePath());
            mediaPlayer.prepare();
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "error preparing media player", e);
            return;
        }
        mediaPlayer.setVolume(0.5f, 0.5f);
        mediaPlayer.start();

        stopRecordingButton.setEnabled(false);
        recordButton.setEnabled(false);
        playButton.setEnabled(false);
        stopPlayingButton.setEnabled(true);
    }

    @OnClick(R.id.stop_playing_button)
    public void stopPlayingButton() {
        mediaPlayer.stop();
        mediaPlayer.release();

        stopRecordingButton.setEnabled(false);
        recordButton.setEnabled(true);
        playButton.setEnabled(true);
        stopPlayingButton.setEnabled(false);
    }
}
