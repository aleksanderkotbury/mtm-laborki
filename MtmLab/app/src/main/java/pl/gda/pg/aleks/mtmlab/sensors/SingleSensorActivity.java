package pl.gda.pg.aleks.mtmlab.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import pl.gda.pg.aleks.mtmlab.R;
import pl.gda.pg.aleks.mtmlab.SensorEventPublisher;

public class SingleSensorActivity extends AppCompatActivity {

    public static final String SENSOR_TYPE = "single_sensor_type";

    @BindView(R.id.sensor_values)
    public ListView listView;
    @BindView(R.id.sensor_type_value)
    public AppCompatTextView sensorType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_sensor);
        ButterKnife.bind(this);
        ArrayAdapter<Float> floatArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(floatArrayAdapter);

        int sensorType = getIntent().getExtras().getInt(SENSOR_TYPE);
        this.sensorType.setText("" + sensorType);


        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensorMagnetic = sensorManager.getDefaultSensor(sensorType);

        Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorMagnetic, 1000);
        }).subscribe(event -> {
            floatArrayAdapter.clear();
            for (float f : event.values) {
                floatArrayAdapter.add(f);
            }
        });
    }
}
